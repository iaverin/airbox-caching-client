package CachingProxy;

import org.apache.log4j.Logger;

import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private static java.sql.Connection connection = null;
    private static DBConnection dbConnectionInstance;
    private static final String PATH = "/var/lib/musproxyd/db";
    private static Logger logger = Logger.getLogger("DBConn");



    public static DBConnection getInstance()
    {
        if(dbConnectionInstance == null)
        {
            synchronized (DBConnection.class)
            {
                if(dbConnectionInstance == null)
                {
                    dbConnectionInstance = new DBConnection();
                }
            }
        }

        return dbConnectionInstance;
    }

    private DBConnection() {
        createConnection();
    }

    public java.sql.Connection getConnection() throws SQLException
    {
        if(connection == null || connection.isClosed())
        {
            createConnection();
        }

        return connection;
    }

    private static void createConnection()
    {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            logger.error("Class for connect to sqlite database not found. Error: ", e);
            return;
        }
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:" + PATH);
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error("Failed to get connection for " + PATH + ". Error: ", e);
            return;
        }
        logger.trace("Connection opened successfully to db: " + PATH);
    }

}
