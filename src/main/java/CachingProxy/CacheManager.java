package CachingProxy;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.*;
import java.util.*;

public class CacheManager {

    private final int READ_CHUNK_SIZE = 196 * 1024; // 196Kb
    private final int STREAMING_CHUNK_SIZE = 8 * 1024; // 8Kb
    private final int TIMEOUT = 1;
    private final int MB_BYTES = 1048576;
    private final int timeoutMillis = 10000;

    String path = "/var/lib/musproxyd/cache";
    Logger logger = Logger.getLogger("Cacher");
    private DBManager dbManager;
    private long cacheLimit;
    private String currentSong = "";
    private String downloadedSong = "";

    private OutputStream baosDownload = null;
    private int downloadedSize = 0;
    private String playingSong = "";
    private boolean isComplete = false;
    private String sourceUrlSaved;
    private OutputStream outSaved;
    private boolean sleepIsOne = false;
    private ServiceContainer serviceContainer;
    private Integer playingPos = -1;
    private WriterThread writerThread = null;
    private Thread writerRunnableThread = null;
    private int maxTimeSuspendMillis = 5*1000;


    public CacheManager(DBManager dbManager, long cacheLimitMb, ServiceContainer serviceContainer) {
        this.dbManager = dbManager;
        this.cacheLimit = cacheLimitMb;
        this.serviceContainer = serviceContainer;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    void updateCache(String sourceUrl, byte[] data, int read, int curRead) {
        CacheEntity entity = dbManager.getBySourceUrl(sourceUrl);
        if (entity != null) {
            try {
                FileOutputStream fos = new FileOutputStream(path + "/" + entity.getLocalFilename(), true);
                //logger.info("Update file " + sourceUrl + ". Write to file stream from " + entity.getDownloadedSize() + " " + curRead + "bytes");
                fos.write(data, read, curRead);
                fos.close();
                dbManager.setCacheEntityDownloadedSize(sourceUrl, entity.getDownloadedSize() + curRead);
            } catch (Exception ex) {
                logger.info("Failed to save file (updating process) with source url " + sourceUrl + ". Error: " + ex.getMessage());
            }
        } else {
            long totalCacheSize = dbManager.getTotalSizeOfCache();
            while ((totalCacheSize + curRead) > cacheLimit * 1024 * 1024) {
                logger.info("Cache limit (" + cacheLimit * 1024 * 1024 + " bytes) size exceeded. Current cache size: " + totalCacheSize + ". Trying to add file of size: " + data.length +
                        " bytes. Deleting oldest files.");
                entity = dbManager.getOldestEntity();
                if (entity != null && !entity.getSourceUrl().equals(sourceUrl)) {
                    String filename = entity.getLocalFilename();
                    File file = new File(path + "/" + filename);
                    if (file.delete()) {
                        logger.info("File " + filename + " successfully deleted");
                        dbManager.deleteFromCache(entity.getSourceUrl());
                    } else {
                        if (!file.exists() || file.isDirectory())
                            dbManager.deleteFromCache(entity.getSourceUrl());
                        else
                            logger.error("Failed to delete file " + filename);
                    }
                    totalCacheSize = dbManager.getTotalSizeOfCache();
                } else
                    break;
            }
            String name = UUID.randomUUID().toString();
            try {
                FileOutputStream fos = new FileOutputStream(path + "/" + name);
                //logger.info("Create file " + sourceUrl + ". Write to file stream from 0 " + curRead + "bytes");
                fos.write(data, 0, curRead);
                fos.close();
            } catch (Exception ex) {
                logger.info("Failed to save file (creating process) with source url " + sourceUrl + ". Error: " + ex.getMessage());
                return;
            }
            entity = new CacheEntity();
            entity.setLocalFilename(name);
            entity.setSourceUrl(sourceUrl);
            entity.setSize(data.length);
            entity.setDownloadedSize(curRead);
            dbManager.addCacheEntity(entity);
        }
    }

    boolean isFailedDownload(String url) {
        return dbManager.isFailed(url);
    }

    void deleteFromFailed(String url) {
        dbManager.deleteFromFailed(url);
    }

    boolean hasInCache(String sourceUrl) {
        CacheEntity entity = dbManager.getBySourceUrl(sourceUrl);
        return (entity != null);
    }

    boolean isFullyDownloaded(String sourceUrl) {
        CacheEntity entity = getEntity(sourceUrl);
        return (entity != null && new Long(Math.round(((double) entity.getDownloadedSize() / (double) entity.getSize()))).equals(1L));
    }

    CacheEntity getEntity(String sourceUrl) {
        if (!hasInCache(sourceUrl))
            return null;
        else
            return dbManager.getBySourceUrl(sourceUrl);
    }

    byte[] getFromCache(String sourceUrl) {
        CacheEntity entity = dbManager.getBySourceUrl(sourceUrl);
        if (entity != null) {
            File file = new File(path + "/" + entity.getLocalFilename());
            FileInputStream fin = null;
            try {
                fin = new FileInputStream(file);
                byte fileContent[] = new byte[(int) file.length()];
                fin.read(fileContent);
                return fileContent;
            } catch (FileNotFoundException e) {
                logger.info("File not found");
            } catch (IOException ioe) {
                logger.info("Exception while reading file " + ioe);
            } finally {
                try {
                    if (fin != null) {
                        fin.close();
                    }
                } catch (IOException ioe) {
                    logger.info("Error while closing stream: " + ioe);
                }
            }

        }
        return null;
    }

    public boolean download(String url, DownloadManager.AbortFlag abortFlag, URL urlObjRes) throws IOException,
            ClassNotFoundException, NoSuchMethodException {
        downloadedSong = url;
        String srcUrl = url;
        HttpURLConnection mHttpCon;
        URL urlObj = new URL(url);
        int counter = 0;
        boolean isOffline = false;
        for (; ; ) {
            counter++;
            if (counter > 5) {
                logger.error("Failed to download file by url: " + srcUrl + ". Timeout(5)");
                StatisticService.getInstance().setNotFoundStatus(url);
                return false;
            }

            if (urlObjRes == null) {
                mHttpCon = (HttpURLConnection) urlObj.openConnection();
                mHttpCon.setReadTimeout(timeoutMillis);
            } else {
                mHttpCon = (HttpURLConnection) urlObjRes.openConnection();
                mHttpCon.setReadTimeout(timeoutMillis);
            }
            mHttpCon.connect();
            int responseCode;
            try {
                responseCode = mHttpCon.getResponseCode();
            } catch (ProtocolException e) {
                logger.error("Failed to open connection: " + e.getMessage());
                StatisticService.getInstance().setNotFoundStatus(url);
                return false;
            }

            if (responseCode == 200) {
                StatisticService.getInstance().setQueueStatus(srcUrl);
                BufferedInputStream in = new BufferedInputStream(mHttpCon.getInputStream());
                int contentLength = Integer.parseInt(mHttpCon.getHeaderField("Content-Length"));
                if (contentLength != 0) {
                    if (contentLength > serviceContainer.getMaxDownloadSizeMb() * MB_BYTES)
                    {
                        logger.error("Content-length of downloaded track > max dowmloaded size = " + serviceContainer.getMaxDownloadSizeMb() + "Mb");
                        return false;
                    }
                    byte[] data = new byte[contentLength];
                    int read = 0;
                    CacheEntity entity = dbManager.getBySourceUrl(srcUrl);
                    if (entity != null && entity.getDownloadedSize() < entity.getSize()) {
                        byte[] searchingFile = getFromCache(srcUrl);
                        System.arraycopy(searchingFile, 0, data, 0,
                                searchingFile.length);
                        read = searchingFile.length;
                        try {
                            int skipped = skip(in, read);
                            if (skipped > 0)
                                logger.error("Failed to skip the beggining of file. Not skipped size is " + skipped);

                        } catch (IOException e) {
                            logger.error("Failed to read content chunk from destination server: " + e.getMessage());
                            serviceContainer.getDownloadManager().setCachingNow(false);
                            return false;
                        }
                    }

                    long startSuspend = 0;

                    while (read < contentLength) {
                        int curRead = 0;
                        int chunk = Math.min(data.length - read, READ_CHUNK_SIZE);

                        try {
                            if (abortFlag.get()) {
                                logger.info("Abort downloading " + url);
                                return false;
                            }
                            curRead = in.read(data, read, chunk);
                        } catch (SocketTimeoutException e) {
                            logger.error("Failed to read content chunk from input stream: " + e.getMessage());

                            try {
                                if (!hasActiveInternetConnection()) {
                                    isOffline = true;
                                    Thread.sleep(5000);
                                } else {
                                    if (isOffline) {
                                        return download(srcUrl, abortFlag, urlObj);
                                    } else
                                        return false;
                                    //return download(srcUrl, abortFlag, urlObj);
                                }

                            } catch (InterruptedException ex) {
                                logger.error("Failed to sleep thread. Offline mode: " + ex.getMessage());
                                return false;
                            }
                        }

                        if (curRead > 0)
                        {
                            if (playingSong.equals(srcUrl)) {
                                if (writerThread == null)
                                {
                                    writerThread = new WriterThread(data, data.length, read, baosDownload);
                                    writerRunnableThread = new Thread(writerThread);
                                    writerRunnableThread.start();
                                }
                                if (downloadedSize == read) {
                                    writerThread.setCurReadToData(data, read, curRead);

                                } else {
                                    writerThread.setCurReadToData(data, downloadedSize, curRead + (read - downloadedSize));
                                }
                                downloadedSize = read + curRead;
                            }

                            updateCache(srcUrl, data, read, curRead);

                            read += curRead;
                            int percent = new Long(Math.round(((double) read / (double) contentLength * 100))).intValue();
                            StatisticService.getInstance().setCached(url, percent);
                        } else
                        {
                            if (startSuspend == 0)
                            {
                                startSuspend = System.currentTimeMillis();
                            }
                            else if (System.currentTimeMillis() - startSuspend > maxTimeSuspendMillis)
                            {
                                logger.error("Failed to read data from input stream: no data can read last 5 seconds. Finish downloading file!");
                                if (playingSong.equals(srcUrl) && writerThread != null)
                                    writerThread.setDownloadedSizeSuspended(read);
                                break;
                            }
                        }
                    }
                    downloadedSize = 0;
                    isComplete = true;
                    logger.trace("Download successfully");
                    return true;
                } else
                    logger.trace("Failed to download");
                    return false;

            } else {
                if (mHttpCon.getResponseCode() == 302 && mHttpCon.getURL() != null) {
                    urlObj = new URL(mHttpCon.getHeaderField("Location"));
                } else {
                    logger.warn("Failed to download file by url: " + srcUrl + ". Response code: " + mHttpCon.getResponseCode());
                    if (mHttpCon.getResponseCode() == 404) {
                        StatisticService.getInstance().setNotFoundStatus(url);
                    }
                    return false;
                }
            }

        }
    }

    public void downloadWithChunkOrPlay(Socket clientSocket) {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            isComplete = false;
            String inputLine = null;
            ArrayList<String> srcHeaders = new ArrayList<String>();
            while ((inputLine = in.readLine()) != null) {
                logger.info(">>> " + inputLine);
                srcHeaders.add(inputLine);
                if (inputLine.trim().isEmpty())
                    break;
            }

            OutputStream out = clientSocket.getOutputStream();
            String sourceUrl = getUrl(srcHeaders.get(0));

            DownloadManager downloadManager = serviceContainer.getDownloadManager();
            Integer currentTrackNum = downloadManager.getTrackNumber(sourceUrl);
            if (!playingPos.equals(-1) && !playingPos.equals(currentTrackNum) && currentSong != null)
                StatisticService.getInstance().setNoneStatus(currentSong);
            if (currentTrackNum != null) {
                playingPos = currentTrackNum;
                downloadManager.deleteOutOfDateTasks(currentTrackNum);
            }
            StatisticService.getInstance().setUrl(sourceUrl);
            StatisticService.getInstance().setPlayStatus(sourceUrl);
            if (writerThread != null) {
                writerThread.setAbortFlag(true);
            }

            byte[] searchingFile = getFromCache(sourceUrl);
            if (searchingFile != null) {
                CacheEntity entity = dbManager.getBySourceUrl(sourceUrl);
                StatisticService.getInstance().setCurrentSongLength(entity.getSize());
                if (entity.getDownloadedSize() < entity.getSize() && downloadedSong.equals(entity.getSourceUrl())) {
                    downloadedSize = entity.getDownloadedSize();
                    int percent = new Long(Math.round(((double) entity.getDownloadedSize() / (double) entity.getSize() * 100))).intValue();
                    StatisticService.getInstance().setCached(sourceUrl, percent);
                    writerThread = null;
                    logger.trace("Play from cache. Downloaded partially.");
                    isComplete = false;
                    baosDownload = out;
                    playingSong = sourceUrl;
                    if (!hasActiveInternetConnection()) {
                        byte data[] = new byte[entity.getSize()];
                        System.arraycopy(searchingFile, 0, data, 0,
                                searchingFile.length);
                        writerThread = new WriterThread(data, data.length, searchingFile.length, out);
                        writerRunnableThread = new Thread(writerThread);
                        writerRunnableThread.start();
                    }
                } else if (entity.getDownloadedSize() == entity.getSize()) {
                    StatisticService.getInstance().setCached(sourceUrl, 100);
                    writerThread = new WriterThread(searchingFile, entity.getSize(), entity.getDownloadedSize(), out);
                    writerRunnableThread = new Thread(writerThread);
                    writerRunnableThread.start();
                    logger.trace("Play from cache. Fully downloaded.");
                    isComplete = true;
                } else {
                    writerThread = null;
                    serviceContainer.getDownloadManager().setCachingNow(true);
                    int percent = new Long(Math.round(((double) entity.getDownloadedSize() / (double) entity.getSize() * 100))).intValue();
                    StatisticService.getInstance().setCached(sourceUrl, percent);
                    try {
                        if (!hasActiveInternetConnection() && !sleepIsOne) {
                            sourceUrlSaved = sourceUrl;
                            outSaved = out;
                            sleepIsOne = true;
                            /*add to writer thread downloaded part of track*/
                            byte data[] = new byte[entity.getSize()];
                                System.arraycopy(searchingFile, 0, data, 0,
                                        searchingFile.length);
                            writerThread = new WriterThread(data, data.length, searchingFile.length, out);
                            writerRunnableThread = new Thread(writerThread);
                            writerRunnableThread.start();

                            while (true) {
                                if (!hasActiveInternetConnection())
                                    Thread.sleep(5000);
                                else {
                                    sourceUrl = sourceUrlSaved;
                                    out = outSaved;
                                    sleepIsOne = false;
                                    break;
                                }
                            }
                        } else {
                            logger.trace("Partially downloaded. Playing now.");
                            sourceUrlSaved = sourceUrl;
                            outSaved = out;
                        }
                    } catch (InterruptedException e) {
                        logger.error("Failed to sleep thread : " + e.getMessage());
                    }

                    if (entity.getDownloadedSize() > 0)
                        doHttpRequest(sourceUrl, out, entity.getDownloadedSize(), entity.getSize()); // from entity.getDownloadedSize() because downloaded bytes range 0.. downloaded-1 !!!
                    else
                        doHttpRequest(sourceUrl, out, 0, 0);

                    isComplete = true;
                }
            } else {
                writerThread = null;
                try {
                    if (!hasActiveInternetConnection() && !sleepIsOne) {
                        sourceUrlSaved = sourceUrl;
                        outSaved = out;
                        sleepIsOne = true;
                        while (true) {
                            if (!hasActiveInternetConnection())
                                Thread.sleep(5000);
                            else {
                                sourceUrl = sourceUrlSaved;
                                out = outSaved;
                                sleepIsOne = false;
                                break;
                            }
                        }
                    } else {
                        logger.trace("Play from internet.");
                        sourceUrlSaved = sourceUrl;
                        outSaved = out;
                    }
                } catch (InterruptedException e) {
                    logger.error("Failed to sleep thread : " + e.getMessage());
                }


                byte[] file = doHttpRequest(sourceUrl, out, 0, 0);
                isComplete = true;
            }
            if (writerRunnableThread != null)
                writerRunnableThread.join();
            if (!currentSong.equals("") && !isFullyDownloaded(currentSong))
                dbManager.addFailedUrl(currentSong);
            currentSong = sourceUrl;
        } catch (Exception e) {
            logger.error("Error occured while processing request: " + e.getMessage());
            isComplete = true;
            if (!currentSong.equals("") && !isFullyDownloaded(currentSong))
                dbManager.addFailedUrl(currentSong);
        }
    }

    private byte[] doHttpRequest(String url, OutputStream upstream, int start, int finish) throws IOException, InterruptedException {
        logger.info("Performing http request to " + url);
        String srcUrl = url;
        HttpURLConnection mHttpCon;
        URL urlObj = new URL(url);
        int counter = 0;
        for (; ; ) {
            counter++;
            if (counter > 5) {
                logger.warn("Failed to cache playing file by url: " + srcUrl + ". Timeout(5)");
                return null;
            }
            if (start != 0 && finish != 0) {
                (mHttpCon = (HttpURLConnection) urlObj.openConnection()).setRequestProperty("Range", "bytes=" + start + "-" + finish);
                mHttpCon.setReadTimeout(timeoutMillis);
            } else {
                mHttpCon = (HttpURLConnection) urlObj.openConnection();
                mHttpCon.setReadTimeout(timeoutMillis);
            }
            if (mHttpCon.getResponseCode() == -1) {
                if (start != 0 && finish != 0) {
                    (mHttpCon = new UniversalURLConnection(urlObj)).setRequestProperty("Range", "bytes=" + start + "-" + finish);
                } else {
                    mHttpCon = new UniversalURLConnection(urlObj);
                }
            }
            mHttpCon.connect();
            logger.info("<<< HTTP status: " + mHttpCon.getResponseCode());
            if ((mHttpCon.getResponseCode() == 200 || mHttpCon.getResponseCode() == 206) &&
                    (mHttpCon.getURL() == null || urlObj.toString().equals(mHttpCon.getURL().toString()))) {
                BufferedInputStream in = new BufferedInputStream(mHttpCon.getInputStream());
                String contentLengthHeader = mHttpCon.getHeaderField("Content-Length");
                if (contentLengthHeader == null)
                    contentLengthHeader = "0";
                int contentLength = Integer.parseInt(contentLengthHeader);
                if (contentLength != 0) {
                    try {
                        return doNormalHttpRequest(url, mHttpCon.getURL(), upstream, start, finish);
                    } catch (Exception e) {
                        logger.error("Failed to play url : " + url + ". Error: " + e.getMessage());
                    }

                } else {
                    doStreamingHttpRequest(in, upstream);
                    return null;//need to close socket
                }
            } else {
                if (mHttpCon.getResponseCode() == 302 && mHttpCon.getURL() != null) {
                    urlObj = new URL(mHttpCon.getHeaderField("Location"));
                } else {
                    if (mHttpCon.getURL() != null && !srcUrl.equals(mHttpCon.getURL().toString())) {
                        urlObj = mHttpCon.getURL();
                    } else {
                        if (mHttpCon.getResponseCode() == 404) {
                            dbManager.addFailedUrl(srcUrl);
                        }
                        logger.warn("Failed to cache playing file by url: " + srcUrl + ". Response code: " + mHttpCon.getResponseCode());
                        return null;
                    }
                }
            }
        }
    }

    private void doStreamingHttpRequest(BufferedInputStream in, OutputStream upstream) {
        logger.info("No content length provided - assuming streaming");
        byte[] data = new byte[STREAMING_CHUNK_SIZE];
        int streamed = 0;
        int previousStreamedLogEntry = 0;

        String headers = "HTTP/1.1 200 OK\r\n\r\n";
        byte[] headerBytes = headers.getBytes();
        try {
            upstream.write(headerBytes);
        } catch (IOException e) {
            logger.error("doStreamingHttpRequest : Failed to write headers to the upstream");
        }

        while (true) {
            int curRead = 0;

            try {
                curRead = in.read(data, 0, STREAMING_CHUNK_SIZE);
            } catch (IOException e) {
                logger.error("Failed to read content chunk from destination server: " + e.getMessage());
                break;
            }

            try {
                upstream.write(data, 0, curRead);
            } catch (IOException e) {
                logger.debug("Failed to transmit content chunk to upstream: " + e.getMessage() +
                        ", upstreamed: " + streamed);
                break;
            }

            streamed += curRead;
            if (streamed - previousStreamedLogEntry >= 1024 * 1024) {
                logger.info("Streamed " + String.format("%s", streamed / 1024.0 / 1024.0) + " Mb");
                previousStreamedLogEntry = streamed;
            }
        }

        logger.info("Streaming http request done. Total size: " + streamed);
    }

    private int skip(InputStream in, int skip) throws IOException {
        if (skip < 0)
            return 0;
        byte[] buffer = new byte[READ_CHUNK_SIZE];
        while (skip > 0) {
            int chunk = Math.min(skip, READ_CHUNK_SIZE);
            int readed = in.read(buffer, 0, chunk);
            skip -= readed;
        }
        return skip;
    }

    private byte[] doNormalHttpRequest(String url, URL urlObjRes, OutputStream upstream, int start, int finish) throws IOException,
            ClassNotFoundException, NoSuchMethodException {
        String srcUrl = url;
        HttpURLConnection mHttpCon;
        URL urlObj = new URL(url);
        int counter = 0;
        boolean isOffline = false;
        boolean isPartial = (start != 0 && finish != 0);
        serviceContainer.getDownloadManager().setCachingNow(true);
        serviceContainer.getDownloadManager().deleteFromTasksWhenPlaying(url);
        serviceContainer.getDownloadManager().setAccessible(true);
        //mainUrl to failed if not complete, because already caching and next url using
        String mainUrl = serviceContainer.getDownloadManager().getMainUrlOfTrack(srcUrl);
        if (mainUrl != null && !isFullyDownloaded(mainUrl) && !isFailedDownload(mainUrl) && !mainUrl.equals(url)) {
            serviceContainer.getDbManager().addFailedUrl(mainUrl);
        }
        for (; ; ) {
            counter++;
            if (counter > 5) {
                logger.error("Failed to get connection for file by url: " + srcUrl + ". Timeout(5)");
                StatisticService.getInstance().setNotFoundStatus(url);
                serviceContainer.getDownloadManager().setCachingNow(false);
                return null;
            }

            if (urlObjRes == null) {
                if (start != 0 && finish != 0) {
                    (mHttpCon = (HttpURLConnection) urlObj.openConnection()).setRequestProperty("Range", "bytes=" + start + "-" + finish);
                    mHttpCon.setReadTimeout(timeoutMillis);
                } else {
                    mHttpCon = (HttpURLConnection) urlObj.openConnection();
                    mHttpCon.setReadTimeout(timeoutMillis);
                }
                if (mHttpCon.getResponseCode() == -1) {
                    if (start != 0 && finish != 0) {
                        (mHttpCon = new UniversalURLConnection(urlObj)).setRequestProperty("Range", "bytes=" + start + "-" + finish);
                    } else {
                        mHttpCon = new UniversalURLConnection(urlObj);
                    }
                }

            } else {
                if (start != 0 && finish != 0) {
                    (mHttpCon = (HttpURLConnection) urlObjRes.openConnection()).setRequestProperty("Range", "bytes=" + start + "-" + finish);
                    mHttpCon.setReadTimeout(timeoutMillis);
                } else {
                    mHttpCon = (HttpURLConnection) urlObjRes.openConnection();
                    mHttpCon.setReadTimeout(timeoutMillis);
                }
                if (mHttpCon.getResponseCode() == -1) {
                    if (start != 0 && finish != 0) {
                        (mHttpCon = new UniversalURLConnection(urlObjRes)).setRequestProperty("Range", "bytes=" + start + "-" + finish);
                    } else {
                        mHttpCon = new UniversalURLConnection(urlObjRes);
                    }
                }
            }
            mHttpCon.connect();

            int responseCode;
            try {
                responseCode = mHttpCon.getResponseCode();
            } catch (ProtocolException e) {
                logger.error("Failed to open connection: " + e.getMessage());
                StatisticService.getInstance().setNotFoundStatus(url);
                serviceContainer.getDownloadManager().setCachingNow(false);
                return null;
            }

            /*
            notready = false;
            conn : httpCon

            while(downloadSize < fileSize) && NOT ABORT THIS FILE
            DO
                if (NOT conn.ready() OR notready)
                    conn.connect(offset)
                    if (not_connect_with_offset(offset))
                        process_situation
                    else
                    ....

                    curRead = conn.read();
                    ....
                    ....
                    if (curRead == 0)
                        notready = true;
            END
            * */

            if (!isPartial && responseCode == 200) {
                BufferedInputStream in = new BufferedInputStream(mHttpCon.getInputStream());
                int contentLength = Integer.parseInt(mHttpCon.getHeaderField("Content-Length"));
                if (contentLength != 0) {
                    StatisticService.getInstance().setCurrentSongLength(contentLength);
                    byte[] data = new byte[contentLength];
                    int read = 0;
                    CacheEntity entity = dbManager.getBySourceUrl(srcUrl);
                    if (entity != null && entity.getDownloadedSize() < entity.getSize()) {
                        byte[] searchingFile = getFromCache(srcUrl);

                        read = searchingFile.length;
                        try {
                            int skipped = skip(in, read);
                            if (skipped > 0)
                                logger.error("Failed to skip the beginning of file. Not skipped size is " + skipped);

                        } catch (IOException e) {
                            logger.error("Failed to read content chunk from destination server: " + e.getMessage());
                            serviceContainer.getDownloadManager().setCachingNow(false);
                            return null;
                        }
                    }
                    if (writerThread == null)
                    {
                        writerThread = new WriterThread(data, data.length, read, upstream);
                        writerRunnableThread = new Thread(writerThread);
                        writerRunnableThread.start();
                    }

                    long startSuspend = 0;
                    while (read < contentLength) {
                        int curRead = 0;
                        int chunk = Math.min(data.length - read, READ_CHUNK_SIZE);

                        try {

                            curRead = in.read(data, read, chunk);
                        } catch (SocketTimeoutException e) {
                            logger.error("Failed to read content chunk from destination server: " + e.getMessage());
                            try {
                                if (!hasActiveInternetConnection()) {
                                    isOffline = true;
                                    while (true) {
                                        if (!hasActiveInternetConnection())
                                            Thread.sleep(5000);
                                        else
                                            break;
                                    }
                                } else {
                                    if (isOffline) {
                                        return doNormalHttpRequest(url, mHttpCon.getURL(), upstream, start, finish);
                                    } else {
                                        serviceContainer.getDownloadManager().setCachingNow(false);
                                        return null;
                                    }
                                    /*Thread.sleep(timeoutMillis);
                                    return doNormalHttpRequest(url, mHttpCon.getURL(), upstream, start, finish);*/
                                }

                            } catch (InterruptedException ex) {
                                logger.error("Failed to sleep thread. Offline mode: " + ex.getMessage());
                                serviceContainer.getDownloadManager().setCachingNow(false);
                                return null;
                            }
                        }

                        if (curRead > 0) {

                            if (writerThread != null)
                                writerThread.setCurReadToData(data, read, curRead);
                            else
                            {
                                serviceContainer.getDownloadManager().setCachingNow(false);
                                return null;
                            }

                            if (contentLength <= serviceContainer.getMaxDownloadSizeMb() * MB_BYTES)
                                updateCache(srcUrl, data, read, curRead);
                            read += curRead;
                            int percent = new Long(Math.round(((double) read / (double) contentLength * 100))).intValue();
                            StatisticService.getInstance().setCached(url, percent);
                            if (startSuspend != 0)
                            {
                                startSuspend = 0L;
                            }
                        } else
                        {
                            if (startSuspend == 0)
                            {
                                startSuspend = System.currentTimeMillis();
                            }
                            else if (System.currentTimeMillis() - startSuspend > maxTimeSuspendMillis)
                            {
                                logger.error("Failed to read data from input stream: no data can read last 5 seconds. Finish process!");
                                if (contentLength > serviceContainer.getMaxDownloadSizeMb() * MB_BYTES)
                                    dbManager.addFailedUrl(srcUrl);
                                if (writerThread != null)
                                    writerThread.setDownloadedSizeSuspended(read);
                                break;
                            }

                        }
                    }

                    logger.info("Http request done. Full response size: Content size: " + data.length);
                    serviceContainer.getDownloadManager().setCachingNow(false);
                    return null;
                }

            } else {
                if (isPartial && responseCode == 206) {
                    BufferedInputStream in = new BufferedInputStream(mHttpCon.getInputStream());
                    int contentLength = Integer.parseInt(mHttpCon.getHeaderField("Content-Length"));//length of partial file!
                    if (contentLength != 0) {
                        StatisticService.getInstance().setCurrentSongLength(finish);
                        byte[] data = new byte[finish];
                        int read = 0;
                        CacheEntity entity = dbManager.getBySourceUrl(srcUrl);
                        if (entity != null && entity.getDownloadedSize() < entity.getSize()) {
                            byte[] searchingFile = getFromCache(srcUrl);
                            System.arraycopy(searchingFile, 0, data, 0,
                                    searchingFile.length);
                            read = 0;
                        }

                        if (writerThread == null)
                        {
                            writerThread = new WriterThread(data, data.length, start, upstream);
                            writerRunnableThread = new Thread(writerThread);
                            writerRunnableThread.start();
                        }

                        long startSuspend = 0;
                        while (read < contentLength) {
                            int curRead = 0;
                            int chunk = Math.min(contentLength - read, READ_CHUNK_SIZE);
                            try {
                                curRead = in.read(data, read + start, chunk);

                            } catch (SocketTimeoutException e) {
                                logger.error("Failed to read content chunk from destination server: " + e.getMessage());
                                try {
                                    if (!hasActiveInternetConnection()) {
                                        isOffline = true;
                                        while (true) {
                                            if (!hasActiveInternetConnection())
                                                Thread.sleep(5000);
                                            else
                                                break;
                                        }
                                    } else {
                                        if (isOffline) {
                                            return doNormalHttpRequest(url, mHttpCon.getURL(), upstream, start, finish);
                                        } else {
                                            serviceContainer.getDownloadManager().setCachingNow(false);
                                            return null;
                                        }
                                        //return doNormalHttpRequest(url, mHttpCon.getURL(), upstream, start, finish);
                                    }

                                } catch (InterruptedException ex) {
                                    logger.error("Failed to sleep thread. Offline mode: " + ex.getMessage());
                                    serviceContainer.getDownloadManager().setCachingNow(false);
                                    return null;
                                }
                            }

                            if (curRead > 0)
                            {
                                if (writerThread != null)
                                    writerThread.setCurReadToData(data, read + start, curRead);
                                else
                                {
                                    serviceContainer.getDownloadManager().setCachingNow(false);
                                    return null;
                                }
                                if (finish <= serviceContainer.getMaxDownloadSizeMb() * MB_BYTES)
                                    updateCache(srcUrl, data, read + start, curRead);
                                read += curRead;
                                int percent = new Long(Math.round(((double) (read + start) / (double) finish * 100))).intValue();
                                StatisticService.getInstance().setCached(url, percent);
                                if (startSuspend != 0)
                                    startSuspend = 0;
                            } else
                            {
                                if (startSuspend == 0)
                                {
                                    startSuspend = System.currentTimeMillis();
                                }
                                else if (System.currentTimeMillis() - startSuspend > maxTimeSuspendMillis)
                                {
                                    logger.error("Failed to read data from input stream: no data can read last 5 seconds. Finish process!");
                                    if (finish > serviceContainer.getMaxDownloadSizeMb() * MB_BYTES)
                                        dbManager.addFailedUrl(srcUrl);
                                    if (writerThread != null)
                                        writerThread.setDownloadedSizeSuspended(read);
                                    break;
                                }

                            }
                        }

                        logger.info("Http request done. Full response size: Content size: " + data.length);
                        serviceContainer.getDownloadManager().setCachingNow(false);
                        return null;
                    }

                }
            }
        }
    }

    private String getUrl(String getCommand) {
        if (!getCommand.startsWith("GET")) {
            logger.error("Incorrect GET command: " + getCommand);
            return getCommand;
        }
        String[] tokens = getCommand.split(" ");
        if (tokens.length > 1)
            return tokens[1];
        else {
            logger.info("Incorrect size of tokens in GET command: " + getCommand);
            return getCommand;
        }
    }

    public boolean hasActiveInternetConnection() {
        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com")).openConnection();
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            return (urlc.getResponseCode() > 0);
        } catch (IOException e) {
            logger.error("Error checking internet connection");
        }
        return false;
    }

}
