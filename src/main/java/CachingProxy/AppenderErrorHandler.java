package CachingProxy;

import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.ErrorHandler;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Created by maria on 05.07.14.
 */
public class AppenderErrorHandler implements ErrorHandler {

    private boolean failed = false;

    public boolean isFailed() {
        return failed;
    }


    @Override
    public void setLogger(Logger logger) {
        failed = true;
    }

    @Override
    public void error(String message, Exception e, int errorCode) {
        failed = true;
    }

    @Override
    public void error(String message) {
        failed = true;
    }

    @Override
    public void error(String message, Exception e, int errorCode, LoggingEvent event) {
        failed = true;
    }

    @Override
    public void setAppender(Appender appender) {
        failed = true;
    }

    @Override
    public void setBackupAppender(Appender appender) {
        failed = true;
    }

    @Override
    public void activateOptions() {
        failed = true;
    }
}
