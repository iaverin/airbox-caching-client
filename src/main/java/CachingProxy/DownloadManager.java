package CachingProxy;

import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.*;

public class DownloadManager {

    private Logger logger = Logger.getLogger("Downld");

    public class AbortFlag {
        private boolean aborted = false;

        public synchronized void set(boolean value) {
            aborted = value;
        }

        public synchronized boolean get() {
            return aborted;
        }
    }


    public static class TaskTrack {
        private String url;
        private Integer trackNumber;

        public TaskTrack(String url, Integer trackNumber) {
            this.url = url;
            this.trackNumber = trackNumber;
        }

        public TaskTrack() {
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getTrackNumber() {
            return trackNumber;
        }

        public void setTrackNumber(Integer trackNumber) {
            this.trackNumber = trackNumber;
        }
    }

    private ScheduledFuture currentScheduledFuture;
    private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private int maxQueueSize;
    private ExecutorService executor;
    private Map<TaskTrack, String> tasks = new LinkedHashMap<TaskTrack, String>();
    private CacheManager cacheManager;
    private AbortFlag aborted = new AbortFlag();
    private int poolSize;
    private boolean isCachingNow;
    private int currentlyExecutedTrackNo = 0;
    private boolean isAccessible = true;
    private Integer lastCachedId = -1;
    private static List<TaskTrack> playlist;

    public synchronized void resume() {

        logger.info("Resume. Download queue size: " + tasks.size());
        executor = Executors.newFixedThreadPool(poolSize);
        if (currentScheduledFuture != null && !currentScheduledFuture.isCancelled())
            currentScheduledFuture.cancel(true);
        launchDownloadThread();
    }

    public synchronized void pause() {
        if (currentScheduledFuture == null || currentScheduledFuture.isCancelled()) {
            logger.warn("No tasks scheduled - pause ignored");
            return;
        }

        logger.info("Paused. Download queue size: " + tasks.size());
        if (currentScheduledFuture != null && !currentScheduledFuture.isCancelled())
            currentScheduledFuture.cancel(true);
        aborted.set(true);
        executor.shutdown();
        logger.info("Finished all threads");
        aborted = new AbortFlag();
    }

    //launch new thread that every 5 sec try to start downloading new url
    public void launchDownloadThread() {
        final Runnable launcher = new Runnable() {
            public void run() {
                TaskTrack taskTrack;
                String mainUrlValue;
                taskTrack = getTaskTrackByInd(0);
                if (taskTrack == null)
                    return;
                mainUrlValue = getByUrl(taskTrack.getUrl());

                if ((cacheManager.isFullyDownloaded(mainUrlValue) || cacheManager.isFailedDownload(mainUrlValue) || mainUrlValue == null)
                        && !isCachingNow() && isAccessible()) {
                    currentlyExecutedTrackNo = taskTrack.getTrackNumber();
                    setCachingNow(true);
                    Runnable secondaryWorker = new WorkerThread(taskTrack.getUrl(), cacheManager, getDownloadManager(), aborted, mainUrlValue);
                    executor.execute(secondaryWorker);
                }

            }
        };
        currentScheduledFuture =
                scheduler.scheduleAtFixedRate(launcher, 10, 5, TimeUnit.SECONDS);
    }

    public List<TaskTrack> getPlaylist() {
        return playlist;
    }

    public void setPlaylist(List<TaskTrack> playlist) {
        this.playlist = playlist;
        List<String> urls = new ArrayList<String>();
        for (TaskTrack track : playlist) {
            urls.add(track.getUrl());
        }
        StatisticService.getInstance().setNewPlaylist(urls, "");
    }

    public DownloadManager(CacheManager cacheManager, int workersCount) {
        this.cacheManager = cacheManager;
        this.poolSize = workersCount;
        isCachingNow = false;
        aborted = new AbortFlag();
        maxQueueSize = 0;
    }

    public int getMaxQueueSize() {
        return maxQueueSize;
    }

    public void setMaxQueueSize(int maxQueueSize) {
        this.maxQueueSize = maxQueueSize;
    }

    public DownloadManager getDownloadManager() {
        return this;
    }

    public synchronized void setCachingNow(boolean cachingNow) {
        isCachingNow = cachingNow;
    }

    public synchronized boolean isCachingNow() {
        return isCachingNow;
    }

    public synchronized void removeQueueElements() {
        tasks.clear();
    }

    public Integer getLastCachedId() {
        return lastCachedId;
    }

    public void setLastCachedId(Integer lastCachedId) {
        this.lastCachedId = lastCachedId;
    }

    private synchronized boolean containsUrl(String url) {
        Set<TaskTrack> tracks = tasks.keySet();
        Iterator it = tracks.iterator();
        while (it.hasNext()) {
            TaskTrack track = (TaskTrack) it.next();
            if (track.getUrl().equals(url))
                return true;
        }
        return false;

    }

    private synchronized String getByUrl(String url) {
        Set<TaskTrack> tracks = tasks.keySet();
        Iterator it = tracks.iterator();
        while (it.hasNext()) {
            TaskTrack track = (TaskTrack) it.next();
            if (track.getUrl().equals(url))
                return tasks.get(track);
        }
        return null;

    }

    private synchronized void removeByUrl(String url) {
        Set<TaskTrack> tracks = tasks.keySet();
        Iterator it = tracks.iterator();
        while (it.hasNext()) {
            TaskTrack track = (TaskTrack) it.next();
            if (track.getUrl().equals(url)) {
                tasks.remove(track);
                break;
            }

        }
    }

    public synchronized void deleteFromTasks(int currentTrackNo) {
        Set<TaskTrack> tracks = tasks.keySet();
        Iterator it = tracks.iterator();
        Set<TaskTrack> toDelete = new HashSet<TaskTrack>();
        while (it.hasNext()) {
            TaskTrack track = (TaskTrack) it.next();
            if (track.getTrackNumber() < currentTrackNo) {
                toDelete.add(track);
            }
        }

        for (TaskTrack track : toDelete) {
            String mainUrl = tasks.get(track);
            tasks.remove(track);
            StatisticService.getInstance().setNoneStatus(track.getUrl());
            if (!tasks.containsValue(mainUrl))
                cacheManager.deleteFromFailed(mainUrl);
        }
    }

    public synchronized String getMainUrlOfTrack(String url)
    {
        if (playlist == null)
            return null;
        for (TaskTrack track : playlist) {
            if (track.getUrl().equals(url))
            {
                return tasks.get(track);
            }
        }
        return null;
    }

    public synchronized void deleteFromTasksWhenPlaying(String url) {
        Set<TaskTrack> tracks = tasks.keySet();
        Iterator it = tracks.iterator();
        TaskTrack obj = null;
        while (it.hasNext()) {
            TaskTrack track = (TaskTrack) it.next();
            if (track.getUrl().equals(url)) {
                obj = track;
                break;
            }
        }

        if (obj != null) {
            String mainUrl = tasks.get(obj);
            tasks.remove(obj);
            if (!tasks.containsValue(mainUrl))
                cacheManager.deleteFromFailed(mainUrl);
        }
    }

    public synchronized void deleteOutOfDateTasks(int currentTrackNo) {
        if (currentlyExecutedTrackNo < currentTrackNo) {
            setCachingNow(false);
            pause();
            setAccessible(false);
            //delete out-of-date tasks
            deleteFromTasks(currentTrackNo);
            setAccessible(true);
            resume();
        } else {
            deleteFromTasks(currentTrackNo);
        }
        // delete from ui queue
        StatisticService.getInstance().setNoneStatusBeforeTrack(currentTrackNo);
    }

    public synchronized void setNewWork(List<TaskTrack> urls) {
        logger.info("New tasks list received. Size: " + urls.size());
        setMaxQueueSize(urls.size() - 1);
        List<String> queueUrls = new ArrayList<String>();
        if (urls.size() != 0) {
            String mainUrl = urls.get(0).getUrl();
            for (int i = 1; i < urls.size(); ++i) {
                if (!containsUrl(urls.get(i).getUrl()) && (StatisticService.getInstance().getStatus(urls.get(i).getUrl()) == null
                        || StatisticService.getInstance().getStatus(urls.get(i).getUrl()).equals(Statistic.Status.NONE)
                        || (StatisticService.getInstance().getStatus(urls.get(i).getUrl()).equals(Statistic.Status.Q)
                        && StatisticService.getInstance().getCachedPers(urls.get(i).getUrl()).equals(0)))) {
                    tasks.put(urls.get(i), mainUrl);
                    queueUrls.add(urls.get(i).getUrl());
                }
            }
            if (tasks.size() > 0)
                resume();

            setLastCachedId(urls.get(urls.size() - 1).getTrackNumber());
            logger.trace("Set last cached id = " + urls.get(urls.size() - 1).getTrackNumber());

            for (String url : queueUrls)
            {
                logger.trace("Set Q status for " + url);
                StatisticService.getInstance().setQueueStatus(url);
            }
        }
    }

    public static Integer getTrackNumber(String url) {
        if (playlist == null)
            return null;
        for (TaskTrack track : playlist) {
            if (track.getUrl().equals(url))
                return track.getTrackNumber();
        }
        return null;
    }

    public static Integer getPos(String url) {
        for (TaskTrack track : playlist) {
            if (track.getUrl().equals(url))
                return playlist.indexOf(track);
        }
        return null;
    }

    public static String getTrackUrl(Integer ind) {
        if (playlist == null)
            return null;
        for (TaskTrack track : playlist) {
            if (track.getTrackNumber().equals(ind))
                return track.getUrl();
        }
        return null;
    }

    public synchronized void setNewTaskWithoutMainUrl(TaskTrack track) {
        logger.info("New task received.");
        tasks.put(track, null);
        if (currentScheduledFuture == null || currentScheduledFuture.isCancelled()) {
            resume();
        }
    }

    public boolean isAccessible() {
        return isAccessible;
    }

    public void setAccessible(boolean isAccessible) {
        this.isAccessible = isAccessible;
    }

    public synchronized String getTaskByInd(int position) {
        List<TaskTrack> l = new ArrayList<TaskTrack>(tasks.keySet());
        if (l.size() > position)
            return l.get(position).getUrl();
        else
            return null;
    }

    public synchronized TaskTrack getTaskTrackByInd(int position) {
        List<TaskTrack> l = new ArrayList<TaskTrack>(tasks.keySet());
        if (l.size() > position) {
            return l.get(position);
        } else
            return null;
    }

    public synchronized void onTaskFinished(String url) {
        logger.info("Task finished. Url: " + url);

        if (containsUrl(url)) {
            String mainUrl = getByUrl(url);
            removeByUrl(url);
            if (!tasks.containsValue(mainUrl))
                cacheManager.deleteFromFailed(mainUrl);
        }
        setCachingNow(false);
    }

    public void startExtraCachedUrl() {
        if (playlist.size() > getLastCachedId() && StatisticService.getInstance().getQueueSize() < maxQueueSize) {
            logger.trace("Start extra url with track number " + (getLastCachedId() + 1));
            String url = playlist.get(getLastCachedId()).getUrl();
            DownloadManager.TaskTrack taskTrack = new DownloadManager.TaskTrack();
            taskTrack.setUrl(url);
            taskTrack.setTrackNumber(getTrackNumber(url));
            setNewTaskWithoutMainUrl(taskTrack);
            StatisticService.getInstance().setQueueStatus(url);
            lastCachedId++;
        } else {
            logger.error("Failed to get " + (getLastCachedId() + 1) + " track; Playlist is over or already has " + maxQueueSize + " elements in queue");
        }

    }

}

