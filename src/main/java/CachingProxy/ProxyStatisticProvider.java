package CachingProxy;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ProxyStatisticProvider {
    private final Integer port = 2356;
    private Logger logger = Logger.getLogger("Stat");

    public void start()
    {
        Thread statisticProviderThread = new Thread(new Runnable() {
            @Override
            public void run() {

                for (;;)
                {
                    try {
                        ServerSocket serverSocket = new ServerSocket();
                        serverSocket.bind(new InetSocketAddress(InetAddress.getByName("127.0.0.1"), port));
                        for (;;)
                        {
                            Socket socket = serverSocket.accept();

                            try {
                                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                                String line;
                                while ((line = in.readLine()) != null) {
                                    if (line.equals("STAT")) {
                                        String result = StatisticService.getInstance().getCacheStatistics();
                                        out.println(result);
                                    } else {
                                        logger.warn("Get unknown command from client: " + line);
                                    }
                                }
                            }
                            catch (Exception er)
                            {
                                logger.error("IO error: " + er.getMessage());
                            }
                        }

                    } catch (IOException e) {
                        logger.error("Failed to provide statistic. Error: " + e.getMessage());
                    }
                }
            }
        });
        statisticProviderThread.start();
    }




}
