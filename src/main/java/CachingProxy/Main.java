package CachingProxy;

import org.apache.log4j.*;
import org.apache.log4j.lf5.viewer.configure.ConfigurationManager;
import org.apache.log4j.spi.ErrorHandler;
import org.apache.log4j.spi.LoggingEvent;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;

public class Main {

    static Logger logger = Logger.getLogger("Main");

    public Main(String args[]) {
        if (Arrays.asList(args).contains("--version")) {
            System.out.println("Version: " + getVersionString());
            return;
        }

        initLogger();

        ServiceContainer serviceContainer = null;
        ProxyStatisticProvider proxyStatisticProvider = new ProxyStatisticProvider();
        proxyStatisticProvider.start();
        try {
            serviceContainer = new ServiceContainer();
        } catch (Exception e) {
            logger.fatal("Failed to initialize app components. Error: " + e.getMessage());
            return;
        }
        serviceContainer.getDbManager().makeDbStructIfNeeded();

        try {
            new ExternalController(serviceContainer).start();
        } catch (Exception er) {
            logger.error("Initialization failed: " + er.getMessage());
            System.exit(12);
        }

        try {
            ServerSocket serverSocket = new ServerSocket(serviceContainer.getConfiguration().getListenPort());
            while (true) {
                logger.trace("Listening for new connections...");
                Socket clientSocket = serverSocket.accept();
                logger.trace("New connection accepted: " + clientSocket.getInetAddress().getHostAddress());
                new Connection(clientSocket, serviceContainer).start();
            }
        } catch (IOException e) {
            logger.error("Failed to get connection: " + e.getMessage());
            System.exit(13);
        }
    }

    private void initLogger() {
        final int maxFileSize = 1024 * 1024 * 10;
        ConsoleAppender consoleAppender = new ConsoleAppender();
        consoleAppender.setName("ConsoleAppender1");
        consoleAppender.setLayout(new PatternLayout("%d | %-5p | %-7c | %-9t | %m%n"));
        consoleAppender.setTarget(ConsoleAppender.SYSTEM_OUT);
        consoleAppender.setThreshold(Level.TRACE);
        consoleAppender.activateOptions();

        RollingFileAppender fileAppender = new RollingFileAppender();
        fileAppender.setName("FileAppender1");
        fileAppender.setFile("/var/log/musproxyd/musproxyd.log");
        fileAppender.setMaxFileSize(Integer.toString(maxFileSize));
        fileAppender.setMaxBackupIndex(99);
        fileAppender.setLayout(new PatternLayout("%d | %-5p | %-7c | %-9t | %m%n"));
        AppenderErrorHandler fileErrHandler = new AppenderErrorHandler();
        fileAppender.setErrorHandler(fileErrHandler);
        fileAppender.setThreshold(Level.TRACE);
        fileAppender.activateOptions();

        Logger.getRootLogger().addAppender(consoleAppender);
        Logger.getRootLogger().addAppender(fileAppender);
        Logger.getRootLogger().setLevel(Level.ALL);

        logger.info("------------------ Server started (version: " + getVersionString() + ") ------------------ ");

        if (fileErrHandler.isFailed()) {
            Logger.getRootLogger().removeAppender(fileAppender);
            logger.warn("Failed to write log to logfile. Using console.");
        } else {
            consoleAppender.setThreshold(Level.WARN);
        }

    }

    private final String getVersionString() {
        Properties prop = new Properties();
        InputStream input = null;
        String versionString = "";

        try {
            InputStream in = getClass().getResourceAsStream("/musproxyd.properties");
            prop.load(in);
            return prop.getProperty("musproxyd_version");
        } catch (IOException ex) {
            logger.fatal("Failed to get musproxyd version from properties: " + ex.getMessage());
            return "unknown";
        }
    }

    public static void main(String[] args) {
        new Main(args);
    }


}
