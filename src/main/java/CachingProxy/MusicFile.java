package CachingProxy;

public class MusicFile {

    private String sourceUrl;
    private byte[] file;

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public byte[] getBytes() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }
}
