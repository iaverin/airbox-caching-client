package CachingProxy;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Statistic {

    Logger logger = Logger.getLogger("Stat");

    private String currentUrl;

    private List<TableElement> statisticTable = new ArrayList<TableElement>();
    private boolean isCompeteFillStatistic = false;

    public boolean isCompeteFillStatistic() {
        return isCompeteFillStatistic;
    }

    public void setCompeteFillStatistic(boolean isCompeteFillStatistic) {
        this.isCompeteFillStatistic = isCompeteFillStatistic;
    }

    public void setNewPlaylist(List<String> urls, String title)
    {
        statisticTable = new ArrayList<TableElement>();
        for (int i = 0; i < urls.size(); ++i)
        {
            String url = urls.get(i);
            TableElement tableElement = new TableElement();
            tableElement.setTrackNumber(i+1);
            tableElement.setUrl(url);
            tableElement.setCached(0);
            tableElement.setStatus(Status.NONE);
            tableElement.setPlaylistName(title);
            statisticTable.add(tableElement);
        }
    }

    public List<TableElement> getStatisticTable() {
        return statisticTable;
    }

    public void setPlayingTrack(Integer trackNumber)
    {
        for (TableElement el : statisticTable)
        {
            if (el.getTrackNumber().equals((trackNumber + 1)))
                el.setStatus(Status.PLAY);
        }
    }

    public void setPlayingStatus(String url)
    {
        boolean isSet = false;
        for (TableElement el : statisticTable)
        {
            if (el.getUrl().equals(url))
            {
                el.setStatus(Status.PLAY);
                isSet = true;
            }
        }
        if (!isSet)
            logger.warn("Failed to set status PLAY for url " + url + ". Element not found in statistics table.");
    }



    public Status getStatus(String url)
    {
        for (TableElement el : statisticTable)
        {
            if (el.getUrl().equals(url))
                return el.getStatus();
        }
        return null;
    }

    public Integer getCachedPers(String url)
    {
        for (TableElement el : statisticTable)
        {
            if (el.getUrl().equals(url))
                return el.getCached();
        }
        return 0;
    }

    public boolean setQueueTrack(String url)
    {
        boolean isSet = false;
        for (TableElement el : statisticTable)
        {
            if (el.getUrl().equals(url) && el.getStatus().equals(Status.NONE))
            {
                el.setStatus(Status.Q);
            }
            if (el.getUrl().equals(url))
            {
                isSet = true;
            }

        }
        if (!isSet)
            logger.warn("Failed to set status Q for url " + url + ". Element not found in table.");
        return isSet;

    }

    public void setNoneStatusBeforeTrack(Integer track)
    {
        for (TableElement el : statisticTable)
        {
            if (el.getTrackNumber() < track && !el.getStatus().equals(Status.NONE))
            {
                el.setStatus(Status.NONE);
                logger.trace("setNoneStatusBeforeTrack() : Set none status to url " + el.getUrl());
            }
        }
    }

    public Integer getQueueSize()
    {
        int count = 0;
        for (TableElement el : statisticTable)
        {
            if (el.getStatus().equals(Status.Q))
                count++;
        }
        return count;
    }

    public void setNotFound(String url)
    {
        for (TableElement el : statisticTable)
        {
            if (el.getUrl().equals(url) && !el.getStatus().equals(Status.PLAY))
                el.setStatus(Status.NOTF);
        }
    }

    public void setNone(String url)
    {
        for (TableElement el : statisticTable)
        {
            if (el.getUrl().equals(url))
            {
                logger.trace("setNone(str): Set none status for url :" + url);
                el.setStatus(Status.NONE);
            }
        }
    }

    public void setNone(Integer track)
    {
        for (TableElement el : statisticTable)
        {
            if (el.getTrackNumber().equals(track))
            {
                el.setStatus(Status.NONE);
                logger.trace("setNone(int): Set none status for url: " + el.getUrl());
            }
        }
    }

    public void setCached(String url, Integer cached)
    {
        for (TableElement el : statisticTable)
        {
            if (el.getUrl().equals(url))
                el.setCached(cached);
        }
    }

    public String getCurrentUrl() {
        return currentUrl;
    }

    public void setCurrentUrl(String currentUrl) {
        this.currentUrl = currentUrl;
    }

    public boolean isContainsPlaying()
    {
        for (TableElement el: statisticTable)
        {
            if (el.getStatus().equals(Status.PLAY))
            {
                return true;
            }
        }
        return false;
    }

    public class TableElement
    {
        private Integer trackNumber;
        private String playlistName;
        private Status status;
        private Integer cached;
        private String url;
        private String title;

        public Integer getTrackNumber() {
            return trackNumber;
        }

        public void setTrackNumber(Integer trackNumber) {
            this.trackNumber = trackNumber;
        }

        public String getPlaylistName() {
            return playlistName;
        }

        public void setPlaylistName(String playlistName) {
            this.playlistName = playlistName;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

        public Integer getCached() {
            return cached;
        }

        public void setCached(Integer cached) {
            this.cached = cached;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public enum Status
    {
        NONE, PLAY, Q, NOTF
    }

}
