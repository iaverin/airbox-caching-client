package CachingProxy;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

public class DBManager {

    private static Logger logger = Logger.getLogger("DB");

    public void makeDbStructIfNeeded()
    {
        java.sql.Connection c = null;
        Statement stmt = null;

        try {
            c = DBConnection.getInstance().getConnection();
            stmt = c.createStatement();
            stmt.executeUpdate("PRAGMA journal_mode = OFF");
            stmt.close();
            stmt = c.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS CACHE " +
                    "(ID INTEGER PRIMARY KEY  AUTOINCREMENT   NOT NULL," +
                    " SOURCE_URL           TEXT    NOT NULL, " +
                    " LOCAL_FILENAME            TEXT     NOT NULL, " +
                    " CACHED_DATE        TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " +
                    " SIZE INTEGER       NOT NULL," +
                    " DOWNLOADED_SIZE INTEGER       NOT NULL," +
                    " NAME TEXT)";
            stmt.executeUpdate(sql);
            stmt.close();
            stmt = c.createStatement();
            sql = "CREATE TABLE IF NOT EXISTS FAILED " +
                    "(ID INTEGER PRIMARY KEY  AUTOINCREMENT   NOT NULL," +
                    " SOURCE_URL           TEXT    UNIQUE NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            logger.trace("Database ready");
        } catch ( Exception e ) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void setCacheEntityDownloadedSize(String url, Integer size)
    {
        java.sql.Connection c = null;
        Statement stmt = null;
        try {
            c = DBConnection.getInstance().getConnection();
            stmt = c.createStatement();
            String sql = "UPDATE CACHE SET DOWNLOADED_SIZE = " + size + " WHERE SOURCE_URL=\""+
                    url+"\"";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch ( Exception e ) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void deleteFromFailed(String url)
    {
        java.sql.Connection c = null;
        Statement stmt = null;
        try {
            c = DBConnection.getInstance().getConnection();

            stmt = c.createStatement();
            String sql = "DELETE FROM FAILED WHERE SOURCE_URL=\"" +
                    url + "\"";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch ( Exception e ) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
        }
        logger.trace("Record deleted successfully from failed urls");
    }

    public void addCacheEntity(CacheEntity entity)
    {
        java.sql.Connection c = null;
        Statement stmt = null;
        try {
            c = DBConnection.getInstance().getConnection();

            stmt = c.createStatement();
            String sql = "INSERT INTO CACHE (SOURCE_URL,LOCAL_FILENAME, SIZE, DOWNLOADED_SIZE) " +
                    "VALUES (\"" + entity.getSourceUrl() + "\",'" + entity.getLocalFilename() + "'," + entity.getSize() + ", " + entity.getDownloadedSize() + ")";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch ( Exception e ) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
        }

    }

    public void addFailedUrl(String url)
    {
        if (isFailed(url))
            return;
        java.sql.Connection c = null;
        Statement stmt = null;
        try {
            c = DBConnection.getInstance().getConnection();

            stmt = c.createStatement();
            String sql = "INSERT INTO FAILED (SOURCE_URL) " +
                    "VALUES (\"" + url + "\")";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch ( Exception e ) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
        }
        logger.trace("Add failed url info successfully");

    }

    public boolean isFailed(String url)
    {
        java.sql.Connection c = null;
        Statement stmt = null;
        try {
            c = DBConnection.getInstance().getConnection();

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM FAILED WHERE SOURCE_URL = \"" + url + "\"" );
            CacheEntity entity = null;

            return rs.next();
        } catch ( Exception e ) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
        }
        return false;
    }

    public long getTotalSizeOfCache()
    {
        java.sql.Connection c = null;
        Statement stmt = null;
        try {
            c = DBConnection.getInstance().getConnection();

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT SUM(SIZE) FROM CACHE" );
            if (rs.next())
            {
                return rs.getLong(1);
            }
            else
            {
                logger.trace("Failed to get total size of cache");
            }
        } catch ( Exception e ) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
        }
        return 0;
    }

    public CacheEntity getOldestEntity()
    {
        java.sql.Connection c = null;
        Statement stmt = null;
        try {
            c = DBConnection.getInstance().getConnection();

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "select * from CACHE order by CACHED_DATE ASC limit 0,1" );
            CacheEntity entity = null;

            if (rs.next()) {
                String  sourceUrl = rs.getString("SOURCE_URL");
                String  localFilename = rs.getString("LOCAL_FILENAME");
                String cachedDate = rs.getString("CACHED_DATE");
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                java.util.Date date = formatter.parse(cachedDate);
                Integer size = Integer.parseInt(rs.getString("SIZE"));
                Integer downloaded = Integer.parseInt(rs.getString("DOWNLOADED_SIZE"));

                entity = new CacheEntity();
                entity.setCachedDate(date);
                entity.setLocalFilename(localFilename);
                entity.setSourceUrl(sourceUrl);
                entity.setSize(size);
                entity.setDownloadedSize(downloaded);
            }
            rs.close();
            stmt.close();
            return entity;
        } catch ( Exception e ) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
        }
        return null;
    }

    public CacheEntity getBySourceUrl(String url)
    {
        java.sql.Connection c = null;
        Statement stmt = null;
        try {
            c = DBConnection.getInstance().getConnection();

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM CACHE WHERE SOURCE_URL = \"" + url + "\"" );
            CacheEntity entity = null;

            if (rs.next()) {
                String  sourceUrl = rs.getString("SOURCE_URL");
                String  localFilename = rs.getString("LOCAL_FILENAME");
                String cachedDate = rs.getString("CACHED_DATE");
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                java.util.Date date = formatter.parse(cachedDate);
                Integer size = Integer.parseInt(rs.getString("SIZE"));
                Integer downloaded = Integer.parseInt(rs.getString("DOWNLOADED_SIZE"));

                entity = new CacheEntity();
                entity.setCachedDate(date);
                entity.setLocalFilename(localFilename);
                entity.setSourceUrl(sourceUrl);
                entity.setSize(size);
                entity.setDownloadedSize(downloaded);
            }
            rs.close();
            stmt.close();
            return entity;
        } catch ( Exception e ) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
        }
        return null;
    }

    public void deleteFromCache(String url)
    {
        java.sql.Connection c = null;
        Statement stmt = null;
        try {
            c = DBConnection.getInstance().getConnection();

            stmt = c.createStatement();
            stmt.executeUpdate( "DELETE FROM CACHE WHERE SOURCE_URL = \"" + url + "\"" );
            stmt.close();
        } catch ( Exception e ) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
        }
    }
}
