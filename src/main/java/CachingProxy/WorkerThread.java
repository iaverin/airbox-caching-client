package CachingProxy;

import org.apache.log4j.Logger;
import sun.misc.Cache;

import java.io.IOException;

public class WorkerThread implements Runnable {

    private String url;
    private CacheManager cacheManager;
    private DownloadManager downloadManager;
    private DownloadManager.AbortFlag abortFlag;
    private String mainUrl;
    private Logger logger = Logger.getLogger("Worker");


    public WorkerThread(String url, CacheManager cacheManager, DownloadManager downloadManager, DownloadManager.AbortFlag abortFlag, String mainUrl) {
        this.url = url;
        this.cacheManager = cacheManager;
        this.downloadManager = downloadManager;
        this.abortFlag = abortFlag;
        this.mainUrl = mainUrl;
    }

    @Override
    public void run() {

        logger.info("Start. Url to cache: " + url);

        if (!cacheManager.hasInCache(url)) {
            try {
                boolean result = cacheManager.download(url, abortFlag, null);
                if (!result)
                {
                    downloadManager.onTaskFinished(url);
                    downloadManager.startExtraCachedUrl();
                }
            } catch (Throwable e) {
                logger.error("Failed to download music by url: " + url + ". Error: " + e.getMessage());
                downloadManager.onTaskFinished(url);
                downloadManager.startExtraCachedUrl();
            }
        }
        else
        {
            CacheEntity entity = cacheManager.getEntity(url);
            Integer downloaded = entity.getDownloadedSize() / entity.getSize() * 100;
            StatisticService.getInstance().setCached(url, downloaded);
            if (downloaded == 100)
            {
                logger.info("File already in cache. Process finished");
            }
            else
            {
                logger.info("File partially in cache. Fully downloading");
                boolean result = false;
                try {
                    result = cacheManager.download(url, abortFlag, null);
                    if (!result)
                    {
                        downloadManager.onTaskFinished(url);
                        downloadManager.startExtraCachedUrl();
                    }
                } catch (Throwable e) {
                    logger.error("Failed to download music by url: " + url + ". Error: " + e.getMessage());
                }
            }

        }

        if (!abortFlag.get())
            downloadManager.onTaskFinished(url);

        logger.info("End process");
    }

    @Override
    public String toString() {
        return this.url;
    }
}
