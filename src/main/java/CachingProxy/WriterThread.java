package CachingProxy;

import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by root on 20.04.15.
 */
public class WriterThread implements Runnable {
    private final int READ_CHUNK_SIZE = 196 * 1024; // 196Kb
    private byte[] data;
    private int totalSizeBytes;
    private int currentSize;
    private OutputStream upstream;
    private Logger logger = Logger.getLogger("Writer");
    private DownloadManager downloadManager;
    private boolean abortFlag = false;
    private int downloadedSizeSuspended = 0;
    private ByteArrayOutputStream byteArrayOutputStream;

    public void setDownloadedSizeSuspended(int downloadedSizeSuspended) {
        this.downloadedSizeSuspended = downloadedSizeSuspended;
    }

    public WriterThread(byte[] data, int totalSizeBytes, int currentSize, OutputStream upstream) {
        this.data = data;
        this.totalSizeBytes = totalSizeBytes;
        this.currentSize = currentSize;
        this.upstream = upstream;
        downloadedSizeSuspended = 0;
        byteArrayOutputStream = new ByteArrayOutputStream();
    }

    public void setCurReadToData(byte[] src, int offset, int size)
    {
        System.arraycopy(src, offset, data, offset,
                size);
        currentSize += size;
    }

    public void setAbortFlag(boolean abortFlag)
    {
        this.abortFlag = abortFlag;
    }

    @Override
    public void run() {
        int read = 0;
        int chunk;

        String headers = "HTTP/1.1 200 OK\r\n" +
                "Content-Length: " + totalSizeBytes + "\r\n" +
                "Connection: keep-alive\r\n" +
                "Content-type:audio/basic\r\n" +
                "Accept-Ranges: bytes\r\n\r\n";

        byte[] headerBytes = headers.getBytes();
        try {
            upstream.write(headerBytes);
            byteArrayOutputStream.write(headerBytes);
        } catch (IOException e) {
            logger.error("WriterThread : Failed to write headers to the upstream");
        }

        while (!abortFlag && ((read < totalSizeBytes && downloadedSizeSuspended == 0)
                || (downloadedSizeSuspended != 0 && read < downloadedSizeSuspended)))
        {
            if (downloadedSizeSuspended == 0)
                chunk = Math.min(data.length - read, READ_CHUNK_SIZE);
            else
                chunk = Math.min(downloadedSizeSuspended - read, READ_CHUNK_SIZE);
            if (read + chunk <= currentSize)
            {
                try {

                    upstream.write(data, read, chunk);
                    byteArrayOutputStream.write(data, read, chunk);
                    read += chunk;
                } catch (IOException e) {
                    logger.debug("Failed to transmit content chunk to upstream: " + e.getMessage() +
                            ", upstreamed: " + read + "/" + totalSizeBytes + ". Probably change track from UI");
                    //downloadManager.setCachingNow(false);
                    return;
                }
            } else
            {
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    logger.debug("Failed to sleep thread while write to upstream data");
                    return;
                }
            }
        }
        logger.debug("Finish to write to the upstream from WriterThread");


    }
}
