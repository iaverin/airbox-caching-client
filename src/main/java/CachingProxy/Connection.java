package CachingProxy;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Map;

class HostAddress {
    private String host;
    private int port;

    public HostAddress(String hostEntry) throws Exception {
        int delimIdx = hostEntry.indexOf(':');
        if (delimIdx == -1) {
            host = hostEntry;
            port = 80;
            return;
        }
        try {
            port = Integer.parseInt(hostEntry.substring(delimIdx + 1));
        }
        catch (Exception er) {
            throw new Exception("Failed to parse host header from HTTP request. Header: " + hostEntry +
                ", error: " + er.getMessage());
        }
        host = hostEntry.substring(0, delimIdx);
    }

    public String getHost() { return host; }
    public int getPort() { return port; }
}

public class Connection extends Thread {

    private static int lastConnectionId = 0;

    private static Logger logger;
    private Socket clientSocket;
    private int connectionId;
    private ServiceContainer serviceContainer;

    public Connection(Socket socket, ServiceContainer serviceContainer) {
        this.serviceContainer = serviceContainer;
        connectionId = ++lastConnectionId;
        if (connectionId < 0) // Avoid overflow and negative values (java has no unsigned integer types)
            connectionId = 0;
        connectionId = connectionId % 100;
        logger = Logger.getLogger("Conn#" + connectionId);
        this.clientSocket = socket;
        setName("Conn#" + connectionId);
        logger.debug("Connection created");
    }

    public void run() {

        serviceContainer.getCacheManager().downloadWithChunkOrPlay(clientSocket);

        try {
            while (true)
            {
                if (!serviceContainer.getCacheManager().isComplete())
                    Thread.sleep(5000);
                else
                    break;
            }
            logger.debug("Connection closed");
            clientSocket.close();
        }
        catch (Exception er) {
            logger.warn("Failed to close socket: " + er.getMessage());
        }
    }
}
