package CachingProxy;


import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ConfigurationReader {
    private static final String PATH = "/etc/musproxyd/musproxyd.conf";
    private static Logger logger = Logger.getLogger("CfgRead");

    public static Configuration readFromFile() throws Exception{
        try {
            FileInputStream fis = new FileInputStream(new File(PATH));
            Properties prop = new Properties();
            prop.load(fis);
            Configuration configuration = new Configuration();
            configuration.setCacheLimitMb(Integer.parseInt(prop.getProperty("cache_limit_mb")));
            configuration.setDownloadWorkersCount(Integer.parseInt(prop.getProperty("download_workers_count")));
            configuration.setListenPort(Integer.parseInt(prop.getProperty("listen_port")));
            return configuration;

        } catch (Exception e) {
            logger.error("Failed to read configuration file " + PATH + ". Error: " + e.getMessage());
            throw e;
        }
    }
}
