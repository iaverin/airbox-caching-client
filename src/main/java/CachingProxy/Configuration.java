package CachingProxy;

public class Configuration {
    private long cacheLimitMb;//in Mb
    private int listenPort;
    private int downloadWorkersCount;

    public long getCacheLimitMb() {
        return cacheLimitMb;
    }

    public void setCacheLimitMb(int cacheLimitMb) {
        this.cacheLimitMb = cacheLimitMb;
    }

    public int getListenPort() {
        return listenPort;
    }

    public void setListenPort(int listenPort) {
        this.listenPort = listenPort;
    }

    public int getDownloadWorkersCount() {
        return downloadWorkersCount;
    }

    public void setDownloadWorkersCount(int downloadWorkersCount) {
        this.downloadWorkersCount = downloadWorkersCount;
    }
}
