package CachingProxy;

public class ServiceContainer {
    private CacheManager cacheManager;
    private DBManager dbManager;
    private DownloadManager downloadManager;
    private Configuration configuration;
    private int maxDownloadSizeMb = 50;

    public ServiceContainer() throws Exception
    {
        configuration = ConfigurationReader.readFromFile();
        dbManager = new DBManager();
        cacheManager = new CacheManager(dbManager, configuration.getCacheLimitMb(), this);
        downloadManager = new DownloadManager(cacheManager, configuration.getDownloadWorkersCount());
    }

    public CacheManager getCacheManager() {
        return cacheManager;
    }

    public DBManager getDbManager() {
        return dbManager;
    }

    public DownloadManager getDownloadManager() {
        return downloadManager;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public int getMaxDownloadSizeMb() {
        return maxDownloadSizeMb;
    }

    public void setMaxDownloadSizeMb(int maxDownloadSizeMb) {
        this.maxDownloadSizeMb = maxDownloadSizeMb;
    }
}
