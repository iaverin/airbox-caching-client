package CachingProxy;

import org.apache.log4j.Logger;

import javax.naming.ldap.Control;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ExternalController extends Thread {

    private final String RunDir = "/var/run/musproxyd/";
    private final String ControlPipeName = RunDir + "control";
    private final int PipeReadInterval = 500;

    private Logger logger;
    private RandomAccessFile pipeFile;
    private ServiceContainer serviceContainer;
    private boolean readList = false;
    private boolean readPlaylist = false;
    private List<DownloadManager.TaskTrack> urls = new ArrayList<DownloadManager.TaskTrack>();
    private List<DownloadManager.TaskTrack> playlistUrls = new ArrayList<DownloadManager.TaskTrack>();

    public ExternalController(ServiceContainer serviceContainer) throws Exception {
        this.serviceContainer = serviceContainer;
        logger = Logger.getLogger("ExtCtl");
        setName("ExtCtl");

        try {
            File theDir = new File(RunDir);
            if (!theDir.exists()) {
                logger.trace("Creating directory " + RunDir);
                theDir.mkdir();
            }

            if (!new File(ControlPipeName).exists()) {
                logger.trace("Creating control pipe " + ControlPipeName);
                Runtime.getRuntime().exec("mkfifo " + ControlPipeName).waitFor();
                Runtime.getRuntime().exec("chmod a+w " + ControlPipeName).waitFor();
            }

            pipeFile = new RandomAccessFile(ControlPipeName, "rw");
        } catch (Exception e) {
            logger.fatal("Failed to open controller pipe: " + e.getMessage());
            throw e;
        }
    }

    public void run() {
        for (;;) {
            try {
                String cmd = pipeFile.readLine();
                //Thread.sleep(PipeReadInterval);
                if (cmd != null) {
                    try
                    {
                        processCommand(cmd);
                    } catch (Exception e)
                    {
                        logger.error("Failed to process command: " + cmd + ". Error: " + e.getMessage());
                    }
                }
            }
            catch (Exception er) {
                logger.error("Error while reading from controlling pipe: " + er.getMessage());
            }
        }
    }


    private void processCommand(String cmd)
    {
        if (cmd.equals("-pause-download-"))
        {
            logger.trace("Received command: " + cmd);
            serviceContainer.getDownloadManager().pause();
            readList = false;
            return;
        }

        if (cmd.equals("-resume-download-"))
        {
            logger.trace("Received command: " + cmd);
            serviceContainer.getDownloadManager().resume();
            readList = false;
            return;
        }

        if (cmd.equals("-begin-list-"))
        {
            logger.trace("Received command: " + cmd);
            readList = true;
            urls = new ArrayList<DownloadManager.TaskTrack>();
            return;
        }

        if (cmd.equals("-end-list-"))
        {
            logger.trace("Received command: " + cmd);
            if (!readList) {
                logger.warn("'-end-list-' received but there was no '-begin-list-' command before");
                return;
            }

            if (urls.size() > 0)
            {
                //tracks number stars with 1
                serviceContainer.getDownloadManager().setNewWork(urls);
            }
            readList = false;
            return;
        }

        if (cmd.equals("-begin-playlist-"))
        {
            logger.trace("Received command: " + cmd);
            readPlaylist = true;
            playlistUrls = new ArrayList<DownloadManager.TaskTrack>();
            return;
        }

        if (cmd.equals("-end-playlist-"))
        {
            logger.trace("Received command: " + cmd);
            if (!readPlaylist) {
                logger.warn("'-end-playlist-' received but there was no '-begin-playlist-' command before");
                return;
            }

            if (playlistUrls.size() > 0)
            {
                serviceContainer.getDownloadManager().setPlaylist(playlistUrls);
            }
            readPlaylist = false;
            return;
        }


        if (cmd.equals("-clean-cache-after-play-"))
        {
            logger.trace("Received command: " + cmd);
            serviceContainer.getDownloadManager().removeQueueElements();
            return;
        }

        if (cmd.startsWith("-cache-trashhold-mb-"))
        {
            logger.trace("Received command: " + cmd);
            int maxMbSize = Integer.parseInt(cmd.substring("-cache-trashhold-mb-".length()));
            serviceContainer.setMaxDownloadSizeMb(maxMbSize);
            return;
        }

        Integer number = Integer.parseInt(cmd.substring(0, cmd.indexOf(" ")));
        String url = cmd.substring(cmd.indexOf(" ") + 1);

        if (readList)
        {
            if (!isContainsUrl(urls, url)) {
                logger.trace("Received command: " + cmd);
                urls.add(new DownloadManager.TaskTrack(url, number));
            } else {
                logger.warn(cmd + " already in list, skipping");
            }
        }
        else if (readPlaylist) {
            if (!isContainsUrl(playlistUrls, url)) {
                logger.trace("Received command: " + cmd);
                playlistUrls.add(new DownloadManager.TaskTrack(url, number));
            }
            else
                logger.warn(cmd + " already in playlist, skipping");
        }
        else
            logger.warn("Unexpected command: " + cmd);
    }

    boolean isContainsUrl(List<DownloadManager.TaskTrack> list, String elem)
    {
        for (DownloadManager.TaskTrack track : list)
        {
            if (track.getUrl().equals(elem))
                return true;
        }
        return false;
    }

}
