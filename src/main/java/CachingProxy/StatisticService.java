package CachingProxy;

import java.util.List;
import org.apache.log4j.Logger;

public class StatisticService {

    private static StatisticService instance;
    private Statistic statistic;
    private Integer currentSongLength;
    private Logger logger = Logger.getLogger("StServ");

    public Integer getCurrentSongLength() {
        return currentSongLength;
    }

    public void setCurrentSongLength(Integer currentSongLength) {
        this.currentSongLength = currentSongLength;
    }

    public static StatisticService getInstance() {

        if (instance == null) {
            instance = new StatisticService();
        }

        return instance;
    }

    public Integer getStatisticTableSize()
    {
        return statistic.getStatisticTable().size();
    }

    public StatisticService() {
        statistic = new Statistic();
    }

    public boolean isCompeteFillStatistic() {
        return statistic.isCompeteFillStatistic();
    }

    public void setCompeteFillStatistic(boolean isCompeteFillStatistic) {
        statistic.setCompeteFillStatistic(isCompeteFillStatistic);
    }

    public void setNewPlaylist(List<String> urls, String title)
    {
        statistic.setNewPlaylist(urls, title);
    }

    public String getCacheStatistics()
    {
        //cacheStatus={{1;play;10%};{2;"Q";100%};{3;"Q";100%};{4;NOTF;0}}
        if (!statistic.isContainsPlaying() && statistic.getCurrentUrl() != null)
        {
            statistic.setPlayingStatus(statistic.getCurrentUrl());
            logger.warn("Add PLAY status to " + statistic.getCurrentUrl());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("SongLength:" + currentSongLength +",\"cacheStatus\":[");
        List<Statistic.TableElement> list = statistic.getStatisticTable();
        boolean isWriting = false;
        for (Statistic.TableElement tableElement : list)
        {
            if (!tableElement.getStatus().equals(Statistic.Status.NONE))
            {
                isWriting = true;
                sb.append("{\"index\":" + tableElement.getTrackNumber() + ",\"state\":\"" + tableElement.getStatus() + "\",\"cache\":" + tableElement.getCached()
                        + "},");

            }
        }
        if (isWriting)
            sb.deleteCharAt(sb.length() - 1);
        sb.append("]");
        return sb.toString();
    }


    public Statistic.Status getStatus(String url)
    {
        return statistic.getStatus(url);
    }

    public Integer getCachedPers(String url)
    {
        return statistic.getCachedPers(url);
    }

    public Integer getQueueSize()
    {
        return statistic.getQueueSize();
    }

    public void setPlayStatus(Integer trackNum)
    {
        statistic.setPlayingTrack(trackNum);
    }

    public void setPlayStatus(String url)
    {
        statistic.setPlayingStatus(url);
    }

    public void setUrl(String url)
    {
        statistic.setCurrentUrl(url);
    }

    public boolean setQueueStatus(String url)
    {
        return statistic.setQueueTrack(url);
    }

    public void setNoneStatus(String url)
    {
        statistic.setNone(url);
    }

    public void setNoneStatus(Integer track)
    {
        statistic.setNone(track);
    }

    public void setNotFoundStatus(String url)
    {
        statistic.setNotFound(url);
    }

    public void setCached(String url, Integer cached)
    {
        statistic.setCached(url, cached);
    }

    public void setNoneStatusBeforeTrack(Integer track)
    {
        statistic.setNoneStatusBeforeTrack(track);
    }

}

