#!/bin/sh
### BEGIN INIT INFO
# Provides:          musproxyd
# Required-Start:    $local_fs $network $remote_fs $syslog
# Required-Stop:     $local_fs $network $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: caching HTTP proxy for mpd
# Description:       caching HTTP proxy for mpd
### END INIT INFO

# Author: Maria Ivashkevich <mivashkevich@gmail.com>

# clear poisonned environment
unset TMPDIR

NAME=music_proxy_daemon.jar
DAEMON=/usr/sbin/$NAME
PIDDIR=/var/run/musproxyd
PIDFILE=$PIDDIR/$NAME.pid

#test -x "$DAEMON" || exit 0

. /lib/lsb/init-functions


case "$1" in
    start)
        log_daemon_msg "Starting music HTTP proxy daemon" "$NAME"
        if [ ! -d $PIDDIR  ]; then
	    mkdir -p $PIDDIR
	fi
        start-stop-daemon --make-pidfile --pidfile "$PIDFILE" --start --quiet --background --exec "/usr/bin/java" -- -jar $DAEMON
        log_end_msg $?
        ;;
    stop)
        log_daemon_msg "Stopping music HTTP proxy daemon" "$NAME"
        start-stop-daemon --pidfile "$PIDFILE" --stop --signal 9 --oknodo  --exec "/usr/bin/java" -- -jar $DAEMON
        log_end_msg $?
        ;;
    restart|force-reload|reload)
        $0 stop
        $0 start
        ;;
    status)
        status_of_proc -p "$PIDFILE" "$DAEMON"
        R=$?
        exit $R
        ;;
    *)
        echo "Usage: /etc/init.d/musproxyd {start|stop|reload|force-reload|restart|status}"
        exit 1
        ;;
esac

exit 0
